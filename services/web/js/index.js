const MOVIES_API_BASE = 'http://127.0.0.1:3000';
const STREAM_API_BASE = 'http://127.0.0.1:3001';


class EventEmitter {
  constructor() {
    this.events = {};
  }

  on(eventName, callback) {
    this.events[eventName] = this.events[eventName] || [];
    this.events[eventName].push(callback);
  }

  emit(eventName) {
    this.events[eventName]
      .forEach(callback => callback());
  }
}

class MovieList extends EventEmitter {

  constructor() {
    super();
    this.query = { page: 1, limit: 25, genre: 'all', query_term: '0' };
    this.movies = [];
    this.count = 0;
    this.root = document.querySelector('#movie-list');
  }

  update(query) {
    Object.assign(this.query, query);
    this.load();
  }

  search(query_term) {
    pagination.info.current = 1;
    this.update({ page: 1, query_term });
  }

  load() {
    const query = `page=${this.query.page}&limit=${this.query.limit}&genre=${this.query.genre}&query_term=${this.query.query_term}`;
    fetch(`${MOVIES_API_BASE}/search?${query}&ts=${Date.now()}`)
      .then(response => response.json())
      .then(data => {
        this.movies = data.data.movies || [];
        this.count = data.data.movie_count;
        this.emit('update');
        window.scrollTo(0, 0);
        this.render();
      })
      .catch(error => window.alert(error));
  }

  render() {
    this.root.innerHTML = '';
    this.movies
      .map(movie => {
        const div = document.createElement('div');
        const a = document.createElement('a');
        const img = document.createElement('img');

        div.classList.add('cell');
        img.classList.add('img-responsive');

        a.href = '#';
        a.addEventListener('click', event => {
          event.preventDefault();

          movie_dialog.setMovie(movie);
        });
        img.src = movie.medium_cover_image;

        a.appendChild(img);
        div.appendChild(a);
        return div;
      })
      .forEach(div => {
        this.root.appendChild(div);
      });
  }
}


class Pagination extends EventEmitter {
  constructor() {
    super();
    this.root = document.querySelector('#pagination');
    this.info = { max: 1000, current: 1 };
  }

  update(max) {
    this.info.max = Math.ceil(max++ / 25);
    this.render();
  }

  change(current) {
    this.info.current = current;
    this.emit('change');
  }

  render() {
    this.root.innerHTML = '';

    if (this.info.current > 1) {
      const prevButton = document.createElement('button');
      prevButton.addEventListener('click', () => this.change(this.info.current - 1));
      prevButton.innerHTML = '&LT; Prev';
      prevButton.classList.add('third');
      this.root.appendChild(prevButton);
    }

    if (this.info.max > 1) {
      const nextButton = document.createElement('button');
      nextButton.addEventListener('click', () => this.change(this.info.current + 1));
      nextButton.innerHTML = 'Next &GT;';
      nextButton.classList.add('third');
      this.root.appendChild(nextButton);
    }
  }
}

class SearchBar extends EventEmitter {
  constructor() {
    super();
    this.root = document.querySelector('#search-bar');
    this.value = this.root.value;
    this.timeout = null;
    this.root.addEventListener('keyup', event => {
      if (event.keyCode == 13)
        this.update();
    });
  }

  update() {
    this.value = this.root.value;
    this.emit('update');
  }
}

class MovieDialog {
  constructor() {
    this.root = document.createElement('div');
    this.modal = document.createElement('div');
    this.titleDiv = document.createElement('div');
    this.title = document.createElement('h2');
    this.content = document.createElement('div');
    this.player = document.createElement('iframe');
    this.video = document.createElement('video');
    this.footer = document.createElement('div');
    this.playButton = document.createElement('button');
    this.closeButton = document.createElement('button');

    this.root.classList.add('dialog');
    this.modal.classList.add('modal');
    this.titleDiv.classList.add('modal-title');
    this.content.classList.add('modal-content');
    this.footer.classList.add('modal-footer');

    this.player.frameBorder = '0';
    this.player.width = '640';
    this.player.height = '360';

    this.video.width = '640';
    this.video.height = '360';
    this.video.controls = 'controls';
    this.video.autoplay = 'autoplay';

    this.playButton.disabled = 'disabled';
    this.playButton.textContent = 'Play';
    this.closeButton.textContent = 'Close';

    this.closeButton.classList.add('second');
    this.closeButton.addEventListener('click', () => this.close());
    this.playButton.addEventListener('click', () => this.play());

    this.titleDiv.appendChild(this.title);
    this.content.appendChild(this.player);
    this.footer.appendChild(this.playButton);
    this.footer.appendChild(this.closeButton);
    this.modal.appendChild(this.titleDiv);
    this.modal.appendChild(this.content);
    this.modal.appendChild(this.footer);
    this.root.appendChild(this.modal);

    this.info = {};
  }

  setMovie(movie) {
    this.title.textContent = movie.title;
    this.player.src = `https://www.youtube.com/embed/${movie.yt_trailer_code}?autoplay=1`;
    document.body.style.overflow = 'hidden';
    this.open();
    fetch(`${STREAM_API_BASE}/start?hash=${movie.torrents[1].hash}`)
      .then(response => response.json())
      .then(info => {
        this.info = info;
        this.playButton.removeAttribute('disabled');
      })
      .catch(error => window.alert(error));
  }

  play() {
    this.video.src = `http://${window.location.hostname}:${this.info.port}${this.info.path}`;
    this.content.replaceChild(this.video, this.player);
  }

  open() {
    document.body.appendChild(this.root);
  }

  close() {
    fetch(`${STREAM_API_BASE}/end`)
      .then(() => { })
      .catch(error => window.alert(error));

    document.body.style.overflow = 'auto';
    document.body.removeChild(this.root);
    this.content.replaceChild(this.player, this.video);
    this.playButton.disabled = 'disabled';
  }
}

const loader = {
  dialog: null,

  init: function () {
    this.dialog = document.createElement('div');
    this.dialog.classList.add('dialog');
    this.loading = document.createElement('div');
    this.loading.classList.add('loader');
    this.dialog.appendChild(this.loading);
  },

  show: function () {
    document.body.style.overflow = 'hidden';
    document.body.appendChild(this.dialog);
  },

  close: function () {
    try {
      document.body.style.overflow = 'auto';
      document.body.removeChild(this.dialog);
    } catch (error) {
      // ignore
    }
  }
};
loader.init();

// setup

const movie_list = new MovieList();
const pagination = new Pagination();
const search_bar = new SearchBar();
const movie_dialog = new MovieDialog();

movie_list.on('update', () => {
  loader.close();
  pagination.update(movie_list.count);
});

pagination.on('change', () => {
  loader.show();
  movie_list.update({ page: pagination.info.current });
});

search_bar.on('update', () => {
  loader.show();
  movie_list.search(search_bar.value);
});

movie_list.load();
