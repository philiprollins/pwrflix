/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */

process.env.NODE_ENV = 'test';

const request = require('supertest');
const server = require('../src/app');
const assert = require('assert');

describe('Stream API routes', function () {
  it('replies pong', function (done) {
    request(server)
      .get('/ping')
      .expect(200)
      .expect('pong')
      .end(done);
  });

  it('starts a stream using a hash', function (done) {
    this.timeout(20 * 1000);
    request(server)
      .get('/start?hash=03B6752B7626FA3E1801F55A2A8F0EB43C814DB1')
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(function (error, response) {
        assert.ok(response.body.port);
        assert.ok(response.body.path.endsWith('mp4'));
      })
      .end(done);
  });

  it('ends the current stream', function (done) {
    request(server)
      .get('/end')
      .expect(200)
      .end(done);
  });
});
