const WebTorrent = require('webtorrent');

const client = new WebTorrent({
  maxConns: 300
});

const announce = [
  'udp://open.demonii.com:1337/announce',
  'udp://tracker.openbittorrent.com:80',
  'udp://tracker.coppersurfer.tk:6969',
  'udp://glotorrents.pw:6969/announce',
  'udp://tracker.opentrackr.org:1337/announce',
  'udp://torrent.gresille.org:80/announce',
  'udp://p4p.arenabg.com:1337',
  'udp://tracker.leechers-paradise.org:6969'
];

let streamingHash = null;
let server = { close: () => { } };
let info = {};

process.on('beforeExit', () => {
  server.close();
  client.destroy();
});

const endStream = () => new Promise(resolve => {
  if (streamingHash != null) {
    client.remove(streamingHash, () => {
      server.close();
      streamingHash = null;
      info = {};
      resolve();
    });
  } else {
    resolve();
  }
});

module.exports = {};

module.exports.end = endStream;

module.exports.stream = hash => new Promise((resolve, reject) => {
  if (streamingHash == hash) {
    resolve(info);
  } else {
    endStream()
      .then(() => {
        client.add(hash, { announce }, torrent => {
          server = torrent.createServer();
          server.listen(3002);

          for (let i in torrent.files) {
            if (torrent.files[i].name.endsWith('mp4')) {
              info.path = `/${i}/${torrent.files[i].name}`;
            }
          }

          if (info.path) {
            streamingHash = hash;
            info.port = server.address().port;
            resolve(info);
          } else {
            client.remove(hash);
            server.close();
            reject('No movie file was located!');
          }
        });
      });
  }
});
