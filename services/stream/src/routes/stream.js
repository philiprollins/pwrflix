const express = require('express');
const stream = require('../modules/stream');

const router = express.Router();

router.get('/ping', (request, response) => {
  response.send('pong');
});

router.get('/end', (request, response) => {
  stream.end()
    .then(() => response.sendStatus(200));
});

router.get('/start', (request, response) => {
  if (request.query.hash && request.query.hash.match(/^[A-Z0-9]{40}$/)) {
    stream.stream(request.query.hash)
      .then(info => response.json(info))
      .catch(error => response.status(500).json({ error }));
  } else {
    response.status(400).json({ error: 'Where is the hash?' });
  }
});

module.exports = router;
