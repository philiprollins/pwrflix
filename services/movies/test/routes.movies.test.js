/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */

process.env.NODE_ENV = 'test';

const request = require('supertest');
const server = require('../src/app');
const assert = require('assert');

describe('Movies API Routes', function () {
  describe('GET /movies/ping', function () {
    it('returns pong', function (done) {
      request(server)
        .get('/movies/ping')
        .expect(200, 'pong')
        .end(done);
    });
  });

  describe('GET /movies/search', function () {
    it('returns a list of movies', function (done) {
      this.timeout(3500);
      request(server)
        .get('/movies/search')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (response) {
          assert.equal(response.body.status, 200);
          assert.equal(response.body.message, 'OK');
          assert.equal(response.body.data.movies.length, 30);
          assert.ok(response.body.data.movies[0].title.length > 1);
        })
        .end(done);
    });
  });

  describe('GET /movies/suggestions', function () {
    it('returns suggestions based on movie id', function (done) {
      request(server)
        .get('/movies/suggestions?movie_id=10')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (response) {
          assert.equal(response.body.status, 200);
          assert.equal(response.body.message, 'OK');
          assert.equal(response.body.data.movie_count, 4);
          assert.equal(response.body.data.movies[0].id, 5989);
        })
        .end(done);
    });
  });

  describe('GET /movies/details', function () {
    it('returns extended details based on movie id', function (done) {
      request(server)
        .get('/movies/details?movie_id=10')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (response) {
          assert.equal(response.body.status, 200);
          assert.equal(response.body.message, 'OK');
          assert.equal(response.body.data.movie.title, '13');
          assert.equal(response.body.data.movie.genres[0], 'Action');
        })
        .end(done);
    });
  });
});
