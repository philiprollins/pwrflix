const https = require('https');
const queryString = require('querystring');
const cache = require('./cache');

const getData = (endPoint, params) => new Promise((resolve, reject) => {
  const options = {
    hostname: 'yts.mx',
    port: 443,
    path: `/api/v2/${endPoint}.json?${queryString.stringify(params)}`,
    method: 'GET'
  };

  cache.get(options.path)
    .then(reply => resolve(reply))
    .catch(() => {
      const request = https.request(options, (response) => {
        const body = [];

        response.on('data', chunk => body.push(chunk));

        response.on('end', () => {
          try {
            const { data } = JSON.parse(Buffer.concat(body).toString());
            cache.set(options.path, data);
            resolve(data);
          } catch (jsonError) {
            reject(new Error('YTS is currently offline, can not fetch movies!'));
          }
        });
      });

      request.on('error', httpError => reject(httpError));

      request.end();
    });
});

module.exports.list = async (query) => {
  const params = Object.assign({}, {
    page: 1,
    limit: 30,
    genre: 'all',
    query_term: 0
  }, query);

  const data = await getData('list_movies', params);
  return data || [];
};

module.exports.suggestions = async (movieId) => {
  const data = await getData('movie_suggestions', { movie_id: movieId });
  return data || [];
};

module.exports.details = async (movieId) => {
  const data = await getData('movie_details', { movie_id: movieId });
  return data || {};
};
