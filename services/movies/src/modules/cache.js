const cache = {};

module.exports.has = (key) => {
  return (typeof cache[key] !== 'undefined');
};

module.exports.set = (key, value) => {
  cache[key] = value;
};

module.exports.get = key => new Promise((resolve, reject) => {
  if (key in cache) {
    resolve(cache[key]);
  } else {
    reject(new Error('No cache entity!'));
  }
});
