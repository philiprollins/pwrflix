const express = require('express');
const ytsAPI = require('../modules/yts-api');

const router = express.Router();

router.get('/ping', (request, response) => {
  response.send('pong');
});

router.get('/search', async (request, response, next) => {
  if (request.query.ts) {
    delete request.query.ts;
  }

  ytsAPI.list(request.query)
    .then((data) => {
      response.json({
        status: 200,
        message: 'OK',
        data
      });
    })
    .catch((error) => {
      next(error);
    });
});

router.get('/suggestions', (request, response, next) => {
  ytsAPI.suggestions(request.query.movie_id)
    .then((data) => {
      response.json({
        status: 200,
        message: 'OK',
        data
      });
    })
    .catch((error) => {
      next(error);
    });
});

router.get('/details', (request, response, next) => {
  ytsAPI.details(request.query.movie_id)
    .then((data) => {
      response.json({
        status: 200,
        message: 'OK',
        data
      });
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
