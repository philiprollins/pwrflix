const http = require('http');
const express = require('./movies/node_modules/express');
const path = require('path');

const moviesApp = require('./movies/src/app');
const streamApp = require('./stream/src/app');
const webApp = express();

webApp.use(express.static(path.join(__dirname, 'web')));

http.createServer(moviesApp).listen(3000);
http.createServer(streamApp).listen(3001);
webApp.listen(80);